const awaitAppConf = () => {
    let clear;
    let count = 0;
    let accessControl = document.querySelector('cells-access-control');
    return new Promise((resolve, reject) => {
      clear = setInterval(()=>{
        count = count + 1;
        if (window.AppConfig && accessControl && accessControl.begin) {
          resolve('ok');
          clearTimeout(clear);
        } else if(count > 80){
          console.log('no ok appConfig');
          reject('noOk');
        }
      }, 500);
    });
  }

document.addEventListener("DOMContentLoaded", async() => {
    try {
        await awaitAppConf();
        console.log('Ready !!', window.AppConfig);
        let accessControl = document.querySelector('cells-access-control');
        accessControl.externalDMZ = "https://ei-community.grupobbva.com/";
        accessControl.internalDMZ = "https://int-corona-pe.work-02.nextgen.igrupobbva";
        accessControl.pathGT = "TechArchitecture/pe/grantingTicket/V02";
        accessControl.appId = "13000105";
        await accessControl.requestUpdate();
        await accessControl.begin();

        const container = document.querySelector('.menu-container');
        const menu = document.querySelector('bbva-web-navigation-menu');
        menu.exitIcon = '';
        menu.exitText= ' ';
        menu.menuItems = [
          {
            groupName: 'Demo App',
            items: [
              {
                eventName: 'employee-event',
                icon: 'coronita:home',
                text: 'Empleados',
                value: 'main',
                selected: true,
              },
              {
                icon: 'coronita:account',
                text: 'Test Servicio',
                value: 'test',
              }
            ],
          }
        ];
      
        container.addEventListener('mouseover', ev => {
          container.setAttribute('expanded', '');
          menu.expanded = true;
        });
      
        container.addEventListener('mouseleave', ev => {
          container.removeAttribute('expanded');
          menu.expanded = false;
        });
      
        container.addEventListener('focusin', ev => {
          container.setAttribute('expanded', '');
          menu.expanded = true;
        });
      
        container.addEventListener('focusout', ev => {
          container.removeAttribute('expanded');
          menu.expanded = false;
        });

        menu.addEventListener('menu-item-click', ({detail})=> {
          console.log('selected-menu', detail.menuItem.value);
          window.dispatchEvent(new CustomEvent('go-page', { 
            detail: detail.menuItem.value,
            bubbles: true, 
            composed: true
          }));
        })

    } catch(e) {
        console.log('Error al iniciar la autenticacion', e);
    }
});