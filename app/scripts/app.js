(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'main': '/',
      'test': '/test-service'
    }
  });
}());
