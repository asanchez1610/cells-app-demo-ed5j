// Import here your LitElement initial components (critical / startup)

import '@bbva-web-components/bbva-core-scoping-ambients-shim';
import '@webcomponents/shadycss/entrypoints/custom-style-interface.js';
import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import '@cells-components/coronita-icons';
import '../../app/elements/componentes/cells-access-control/cells-access-control.js';
import '../../app/elements/componentes/cells-generic-service-dm/cells-generic-service-dm.js';
import '@bbva-web-components/bbva-web-navigation-menu/bbva-web-navigation-menu.js';
