/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`.content-master-page {
  width: calc(100% - 4rem);
  margin-left: 4rem;
}
`;