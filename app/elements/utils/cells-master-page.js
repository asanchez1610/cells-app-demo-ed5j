import { CellsPage } from '@cells/cells-page';
import { BbvaCoreIntlMixin } from '@bbva-web-components/bbva-core-intl-mixin';
import { html } from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import stylesPages from './cells-master-page-styles';

const bbvaCoreMixin = BbvaCoreIntlMixin;
export class CellsMasterPage extends bbvaCoreMixin(CellsPage) {
  constructor() {
    super();
  }

  element(selector) {
    return this.shadowRoot.querySelector(selector);
  }

  elementsAll(selector) {
    return this.shadowRoot.querySelectorAll(selector);
  }

  firstUpdated() {
    window.addEventListener('go-page', ({ detail }) => {
      console.log('direccionar a la pagina', detail);
      this.navigate(detail);
    });
  }

  request(settings) {
    return new Promise((resolve, reject) => {
      let settingsAwait = { ...settings };
      settingsAwait.onSuccess = (response) => {
        resolve(response);
      };
      settingsAwait.onError = (error) => {
        reject(error);
      };
      this.dispatch('on-send-request', settingsAwait);
    });
  }

  dispatch(_evtName, _detail) {
    const eventDispatch = new CustomEvent(_evtName, {
      detail: _detail,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(eventDispatch);
  }

  content(contentHtml) {
    return html` <cells-template-paper-drawer-panel mode="seamed">
      <div slot="app__main" class="content-master-page">
        <!-- Content HTML -->
        ${contentHtml}
      </div>
    </cells-template-paper-drawer-panel>`;
  }
}

export { stylesPages };
