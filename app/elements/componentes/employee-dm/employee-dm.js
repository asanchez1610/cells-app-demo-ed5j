import { LitElement } from "lit-element";

class EmployeeDm extends LitElement {

  async saveEMployee(employee) {
    let objHeaders = new Headers();
    objHeaders.append("Content-Type", "application/json");

    let body = JSON.stringify(employee);

    var requestOptions = {
      method: "POST",
      headers: objHeaders,
      body: body
    };

    const _response = await fetch("https://backend-soft-dk.com/employees", requestOptions).then(response => response.json());
    return _response;
  }

  async listEmployee() {
    let requestOptions = {
      method: 'GET'
    };
    const _respose = await fetch("https://backend-soft-dk.com/employees?_sort=id:desc", requestOptions).then(response => response.json());
    return _respose;
  }

}
window.customElements.define('employee-dm', EmployeeDm);
