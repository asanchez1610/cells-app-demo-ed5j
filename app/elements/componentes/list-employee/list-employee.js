import { LitElement, html, css } from 'lit-element';
import { bbvaTrash, bbvaEdit } from '@bbva-web-components/bbva-foundations-icons';
import '@bbva-web-components/bbva-icon/bbva-icon';
import '@bbva-web-components/bbva-web-navigation-pagination/bbva-web-navigation-pagination';

class ListEmployee extends LitElement {
    static get styles() {
        return css`
        .styled-table {
            border-collapse: collapse;
            font-size: 0.9em;
            width: calc(100% - 30px);
            margin: 25px 15px 5px 15px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
          }
          
          .styled-table thead tr {
            background-color: #1464A5;
            color: #ffffff;
            text-align: left;
          }
          
          .styled-table th,
          .styled-table td {
              padding: 12px 15px;
          }
          
          .styled-table tbody tr {
            border-bottom: 1px solid #dddddd;
          }
          
          .styled-table tbody tr:nth-of-type(even) {
            background-color: #f3f3f3;
          }
          
          .styled-table tbody tr:last-of-type {
            border-bottom: 2px solid #1464A5;
          }
          
          .styled-table tbody tr.active-row {
            font-weight: bold;
            color: #1464A5;
          }
          .content-icons {
            display:flex;
            align-items: center;
            column-gap: 10px;
          }

          .content-icons bbva-icon {
            cursor: pointer;
          }

          .content-icons .edit {
            color: #01579b;
            transition: all 0.5ms ease-in-out;
          }

          .content-icons .edit:hover {
            color: #0288d1;
          }

          .content-icons .delete {
            color: #616161;
            transition: all 0.5ms ease-in-out;
          }

          .content-icons .delete:hover {
            color: #212121;
          }

          .content-pagination {
            width: calc(100% - 40px);
            padding: 12px 20px;
          }

        `
    }

    static get properties() {
        return { employees: Array };
    }

    constructor() {
        super();
        this.employees = [];
    }

    sendPagination({detail}) {
        console.log('sendPagination', detail);
    }

    render() {
        return html`
        <table class="styled-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Tipo documento</th>
                    <th>Número documnento</th>
                    <th>Sueldo</th>
                </tr>
            </thead>
            <tbody>
                ${this.employees.map(item => html`
                
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.secondName}</td>
                    <td>${item.documentType}</td>
                    <td>${item.documentNumber}</td>
                    <td>${item.amount}</td>
                </tr>

                `)}
                
            </tbody>
        </table>
        `
    }
}
customElements.define('list-employee', ListEmployee);