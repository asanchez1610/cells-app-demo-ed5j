import { LitElement } from 'lit-element';

class CellsGenericServiceDm extends LitElement {
  static get properties() {
    return {
      eventRequestName: String,
      accessControlCmp: String,
    };
  }

  constructor() {
    super();
    this.eventRequestName = 'on-send-request';
    this.accessControlCmp = '';
    this.addListener();
  }

  async addListener() {
    await this.updateComplete;
    window.addEventListener(this.eventRequestName, async (e) => {
      if (e.detail) {
        this.sendRequest(e.detail);
      } else {
        console.error('Config no available', e);
      }
    });
  }

  dispatch(nameEvt, detail) {
    const val = typeof detail === 'undefined' ? null : detail;
    this.dispatchEvent(
      new CustomEvent(nameEvt, {
        composed: true,
        bubbles: true,
        detail: val,
      })
    );
  }

  extract(data, keys, value) {
    let ret;
    if (!this.isEmpty(data) && !this.isEmpty(keys)) {
      let split = keys.split('.');
      ret = data[split.shift()];
      while (ret && split.length) {
        ret = ret[split.shift()];
      }
    }
    return this.isEmpty(ret) && value !== null ? value : ret;
  }

  isEmpty(evaluate) {
    switch (typeof evaluate) {
      case 'object':
        return evaluate === null || Object.keys(evaluate).length === 0;
      case 'string':
        return evaluate === '';
      case 'undefined':
        return true;
      default:
        return false;
    }
  }

  buildUrlRequest(_host, _path) {
    return `${_host}/${_path}`;
  }

  configHeaders(config) {
    let headers = {};
    if (config.contentType) {
      headers['Content-Type'] = config.contentType;
    } else {
      headers['Content-Type'] = 'application/json; charset=utf-8';
    }
    headers['contactId'] = window.sessionStorage.getItem(
      'architectureContactId'
    );
    let tokenName = config.tokenName || 'tsec';
    headers[tokenName] = window.sessionStorage.getItem(tokenName);
    if (config.addHeaders && config.addHeaders.length > 0) {
      config.addHeaders.forEach((element) => {
        headers[element.key] = element.value;
      });
    }
    return headers;
  }

  async sendRequest(config) {
    config = config || {};
    let onSuccess = config.onSuccess || function () {};
    let onError = config.onError || function () {};
    const url = this.buildUrlRequest(config.host, config.path);
    config.method = this.extract(config, 'method', 'GET');
    if (typeof config.intentos === 'undefined') {
      config.intentos = 1;
    } else {
      config.intentos = config.intentos + 1;
    }
    const headers = this.configHeaders(config);
    let settings = {
      method: config.method,
      headers: headers,
      mode: 'cors',
    };
    if (config.body) {
      let tempBody;
      if (typeof config.body === 'object') {
        tempBody = JSON.stringify(config.body);
      } else {
        tempBody = config.body;
      }
      settings.body = tempBody;
    }

    let request = new Request(url, settings);

    fetch(request)
      .then((response) => {
        if (response.ok) {
          response
            .json()
            .then((result) => {
              if (onSuccess) {
                onSuccess({ detail: result, request: response });
              }
            })
            .catch((e) => {
              if (onSuccess) {
                onSuccess({ detail: {}, request: response });
              }
            });
        } else {
          if (response.status === 404) {
            let dataError = {
              status: 404,
              systemErrorCause: '404 - Servicio no disponible',
              systemErrorDescription: `No se puede acceder al url: ${url}`,
            };
            if (onError) {
              onError(dataError);
            }
          } else {
            response
              .json()
              .then((error) => {
                if (this.isExpiredToken(error)) {
                  if (config.intentos < 4) {
                    this.refreshToken(config);
                  } else {
                    error = this.normalizeObjects(error);
                  }
                } else {
                  error = this.normalizeObjects(error);
                  if (onError) {
                    onError(error);
                  }
                }
              })
              .catch((e) => {
                if (onError) {
                  onError(e);
                }
              });
          }
        }
      })
      .catch((e) => {
        console.log('Error sendRequest', e);
        if (onError) {
          onError({
            status: 400,
            systemErrorCause: 'Error insesperado!',
            systemErrorDescription: `Ha ocurrido un error inesperado`,
          });
        }
      });
  }

  async refreshToken(config) {
    let accessControl = document.querySelector(this.accessControlCmp);
    if (accessControl) {
      await accessControl.init();
      await this.sendRequest(config);
    }
  }

  isExpiredToken(error) {
    console.log('isExpiredToken', error);
    let isErrorExpiredTsec = false;
    if (error) {
      let errorCode = error['error-code'] || error.errorCode;
      if (
        error.messages &&
        error.messages[0] &&
        error.messages[0].code === 'unauthorized'
      ) {
        errorCode = '68';
      }
      const codesError = {
        tsecExpired: '68',
        tsecinvalid: '61',
        tsecNotFound: '50',
      };
      switch (errorCode) {
        case codesError.tsecExpired:
        case codesError.tsecinvalid:
        case codesError.tsecNotFound:
          isErrorExpiredTsec = true;
          break;
      }
    }
    return isErrorExpiredTsec;
  }

  normalizeObjects(obj) {
    const capitalize = (s) => {
      return s.charAt(0).toUpperCase() + s.slice(1);
    };
    const camelTransform = (text) => {
      let arr = text.split('-');
      let textcamel = '';
      arr.forEach((item, index) => {
        if (index === 0) {
          textcamel = item.toLowerCase();
        } else {
          textcamel = textcamel.concat(capitalize(item));
        }
      });
      return textcamel;
    };
    let responseTransform = {};
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        responseTransform[camelTransform(key)] = obj[key];
      }
    }
    return responseTransform;
  }
}
customElements.define('cells-generic-service-dm', CellsGenericServiceDm);
