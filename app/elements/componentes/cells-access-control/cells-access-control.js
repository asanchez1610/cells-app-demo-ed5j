import { LitElement, html } from "lit-element";
import "@bbva-pe-web-components/bbva-core-employee-authentication/bbva-core-employee-authentication";

class CellsAccessControl extends LitElement {
  static get properties() {
    return {
      externalDMZ: String,
      internalDMZ: String,
      aapId: String,
      pathGT: String,
    };
  }

  dispatch(_evtName, _detail) {
    const eventDispatch = new CustomEvent(_evtName, {
      detail: _detail,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(eventDispatch);
  }

  async successAuth({ detail }) {
    window.sessionStorage.setItem("tsec", detail.tsec);
    window.sessionStorage.setItem("userId", detail.userId);
  }

  ticketError({ detail }) {
    this.evaluateError(
      `Ha ocurrido un conflicto en servicio de Granting Ticket: ${this.internalDMZ}`,
      detail
    );
  }

  idpError({ detail }) {
    console.log("idpError", detail);
    window.location = this.externalDMZ;
  }

  idpAssertionError({ detail }) {
    this.evaluateError("Ha ocurrido un error de IDP Assertion.", detail);
    window.location = this.externalDMZ;
  }

  ssoError({ detail }) {
    this.evaluateError("Ha ocurrido un error de SSO.", detail);
  }

  evaluateError(message, code) {
    console.log("evaluateError", code);
    this.dispatch("error-authentication", { message, code });
  }

  async begin() {
    console.log("Begin!");
    const employeeAuthCmp = this.shadowRoot.querySelector("#employeeAuth");
    if (!employeeAuthCmp) {
      return;
    }
    employeeAuthCmp.init();
  }

  async init() {
    await employeeAuthCmp.init();
  }

  render() {
    return html`<bbva-core-employee-authentication
      id="employeeAuth"
      granting-ticket-url="${this.internalDMZ}/${this.pathGT}"
      aap-id="${this.appId}"
      @employee-authentication-success="${this.successAuth}"
      @granting-ticket-error="${this.ticketError}"
      @idp-error="${this.idpError}"
      @idp-assertion-error="${this.idpAssertionError}"
      @sso-error="${this.ssoError}"
    ></bbva-core-employee-authentication>`;
  }
}
customElements.define("cells-access-control", CellsAccessControl);
