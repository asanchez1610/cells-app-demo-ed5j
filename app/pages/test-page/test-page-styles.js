/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
}

:host([hidden]),
[hidden] {
  display: none !important;
}

pre.code code {
  margin: 0px 0px 0px 80px;
}

pre.code label {
  width: 80px;
}

main {
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  column-gap: 10px;
  padding: 15px;
  width: calc(100% - 60px);
  margin: 10px 15px;
}
main .form {
  width: 500px;
}
main .form bbva-form-field {
  margin-bottom: 5px;
}
main .form .run-action {
  display: flex;
  align-items: center;
  column-gap: 5px;
}
main .form .run-action * {
  width: 100%;
}
main .form bbva-web-form-textarea {
  margin-top: 5px;
  outline: none;
}
main .content-result {
  width: calc(100% - 400px);
}

.textarea-body {
  width: calc(100% - 10px);
  height: calc(100vh - 200px);
  color: #8F7AE5;
  background-color: #353A35;
  border: none;
  outline: none;
  padding: 10px;
  font-size: 15px;
  font-weight: bold;
  resize: none;
}

.content-result .progress-result {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 5px;
  margin-top: 10px;
}
.content-result .progress-result span:first-child {
  color: #028484;
}
.content-result .progress-result span:last-child {
  color: #C0475E;
}
.content-result .list-info-result {
  width: calc(100% - 30px);
  height: calc(100vh - 415px);
  overflow-x: hidden;
  overflow-y: auto;
  border: 1px solid #e1e1e1;
  padding: 15px;
}
.content-result .list-info-result .item {
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #e1e1e1;
  padding-bottom: 5px;
  margin-bottom: 10px;
}
.content-result .list-info-result .item .ok {
  font-weight: bold;
  color: #028484;
  font-size: 0.9em;
}
.content-result .list-info-result .item .noOk {
  font-size: 0.9em;
  font-weight: bold;
  color: #C0475E;
}

.hide {
  display: none !important;
}

@media only screen and (max-width: 990px) {
  bbva-button-default {
    width: 100%;
    max-width: 100%;
  }
}
pre {
  background: #333;
  white-space: pre;
  word-wrap: break-word;
  overflow: auto;
}

pre.code {
  margin: 0;
  border-radius: 4px;
  border: 1px solid #292929;
  position: relative;
}

pre.code label {
  font-family: sans-serif;
  font-weight: bold;
  font-size: 13px;
  color: #ddd;
  position: absolute;
  left: 1px;
  top: 15px;
  text-align: center;
  width: 85px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  pointer-events: none;
}

pre.code code {
  font-family: "Inconsolata", "Monaco", "Consolas", "Andale Mono", "Bitstream Vera Sans Mono", "Courier New", Courier, monospace;
  display: block;
  margin: 0 0 0 85px;
  padding: 15px 15px 16px 14px;
  border-left: 1px solid #555;
  overflow-x: auto;
  font-size: 13px;
  line-height: 19px;
  color: #ddd;
}

.pre::after {
  content: "double click to selection";
  padding: 0;
  width: auto;
  height: auto;
  position: absolute;
  right: 18px;
  top: 14px;
  font-size: 12px;
  color: #ddd;
  line-height: 20px;
  overflow: hidden;
  -webkit-backface-visibility: hidden;
  transition: all 0.3s ease;
}

pre:hover::after {
  opacity: 0;
  visibility: visible;
}

pre.code-css code {
  color: #91a7ff;
}

pre.code-html code {
  color: #aed581;
}

pre.code-javascript code {
  color: #ffa726;
}

pre.code-json code {
  color: #B6A8EE;
}

pre.code-jquery code {
  color: #4dd0e1;
}
`;