import { html } from 'lit-element';
import '@bbva-web-components/bbva-header-main';
import styles from './test-page-styles.js';
import '@bbva-web-components/bbva-spinner/bbva-spinner';
import {
  CellsMasterPage,
  stylesPages,
} from '../../../app/elements/utils/cells-master-page';
import '@bbva-web-components/bbva-form-field/bbva-form-field';
import '@bbva-web-components/bbva-button-default/bbva-button-default';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select';
import '@bbva-web-components/bbva-web-form-textarea/bbva-web-form-textarea';
/* eslint-disable new-cap */
class TestPage extends CellsMasterPage {
  static get is() {
    return 'test-page';
  }

  render() {
    return this.content(html`
      <div slot="app__header">
        <bbva-header-main text="Test de servicios"> </bbva-header-main>
        <main>
          <div class="form">
            <bbva-form-field
              name="host"
              class="i-test"
              label="Host"
              optional-label=""
            ></bbva-form-field>
            <bbva-form-field
              name="path"
              label="Path"
              class="i-test"
              optional-label=""
            ></bbva-form-field>

            <div class="run-action">
              <bbva-web-form-select name="method" label="Método" class="i-test">
                <bbva-web-form-option value="GET" selected
                  >GET</bbva-web-form-option
                >
                <bbva-web-form-option value="POST">POST</bbva-web-form-option>
                <bbva-web-form-option value="PATCH">PATCH</bbva-web-form-option>
                <bbva-web-form-option value="PUT">PUT</bbva-web-form-option>
                <bbva-web-form-option value="DELETE"
                  >DELETE</bbva-web-form-option
                >
                <bbva-web-form-option value="OPTIONS"
                  >OPTIONS</bbva-web-form-option
                >
              </bbva-web-form-select>

              <bbva-button-default
                class="btn-full-width"
                text="Probar"
                @click="${this.testService}"
              ></bbva-button-default>
            </div>

            <bbva-web-form-textarea
              name="body"
              label="Body"
              class="i-test"
            ></bbva-web-form-textarea>
          </div>
          <div class="content-result">
            <pre
              class="code code-json"
            ><label>Response</label><code id="code-result">{}</code></pre>
            <hr />
            <pre
              class="code code-json"
            ><label>Headers</label><code id="code-header">{}</code></pre>
          </div>
        </main>
      </div>
    `);
  }

  onPageEnter() {
    // Cada vez que accedamos a la pagina se ejecuta
    this.test();
  }

  async test() {
    const settings = {
      path: 'accounting/v0/conciliations/accounts/online-conciliation/current-status-process?executionProcessDate=2023-02-20T00:00:00',
      host: 'https://int-corona-pe.work-02.nextgen.igrupobbva',
    };
    await this.request(settings);
  }

  async testService() {
    const inputs = this.elementsAll('.i-test');
    console.log('testService[inputs]', inputs);
    this.element('#code-result').innerHTML = 'Procesando...';
    this.element('#code-header').innerHTML = 'Procesando...';
    let settings = {};
    inputs.forEach((input) => {
      if (input.name && input.value) {
        settings[input.name] = input.value.trim();
      }
    });
    console.log('testService[settings]', settings);
    const _response = await this.request(settings);
    console.log('testService[_response]', _response);
    let jsonHeaders = {};
    _response.request.headers.forEach((value, _name) => {
      if (_name !== 'tsec') {
        jsonHeaders[_name] = value;
      }
    });
    console.log(jsonHeaders);
    this.element('#code-result').innerHTML = JSON.stringify(
      _response.detail,
      null,
      2
    );
    this.element('#code-header').innerHTML = JSON.stringify(
      jsonHeaders,
      null,
      2
    );
  }

  onPageLeave() {
    // Cada vez que salgamos de la pagina se ejecuta
  }

  static get styles() {
    return [styles, stylesPages];
  }
}

window.customElements.define(TestPage.is, TestPage);
