import { html } from 'lit-element';
import '@bbva-web-components/bbva-header-main';
import styles from './main-page-styles.js';
import '@ed5j-components/cells-formulario-ed5j/cells-formulario-ed5j';
import '@bbva-web-components/bbva-spinner/bbva-spinner';
import '../../../app/elements/componentes/employee-dm/employee-dm';
import '../../../app/elements/componentes/list-employee/list-employee';
import {
  CellsMasterPage,
  stylesPages,
} from '../../../app/elements/utils/cells-master-page';
import '@bbva-web-components/bbva-spinner/bbva-spinner';
/* eslint-disable new-cap */
class MainPage extends CellsMasterPage {
  static get is() {
    return 'main-page';
  }

  async register({ detail }) {
    console.log('Datos', detail);
    try {
      this.element('bbva-spinner').classList.remove('hide');
      const data = await this.employeeDM.saveEMployee(detail);
      this.element('bbva-spinner').classList.add('hide');
      this.element('cells-formulario-ed5j').resetForm();
      this.element('bbva-web-notification-toast').opened = true;
      this.element(
        'bbva-web-notification-toast'
      ).innerHTML = `${data.name}, se ha registrado de forma correcta !`;
      this.element('bbva-web-notification-toast').variant = 'success';
      this.loadEmployees();
    } catch (e) {
      this.element('bbva-spinner').classList.add('hide');
      console.log('Error al registrar', e);
    }
  }

  render() {
    return this.content(html`
      <bbva-spinner with-mask="" class="loading hide"></bbva-spinner>
      <bbva-header-main text="Modulo de Empleados"> </bbva-header-main>

      <div class="container">
        <div class="content-form">
          <h2>Registro de empleado</h2>
          <cells-formulario-ed5j
            @on-form-employee-data="${this.register}"
            .documentsTypes="${this.documentsType}"
          ></cells-formulario-ed5j>
        </div>
        <div class="content-list">
          <list-employee></list-employee>
        </div>
        <!-- DM de empleados -->
        <employee-dm></employee-dm>
        <!-- Toast de mensajes -->
        <bbva-web-notification-toast></bbva-web-notification-toast>
      </div>
      <!-- Spinner pages -->
    
    `);
  }

  get employeeDM() {
    return this.shadowRoot.querySelector('employee-dm');
  }

  get documentsType() {
    return window.AppConfig.documentsType;
  }

  onPageEnter() {
    // Cada vez que accedamos a la pagina se ejecuta
    console.log('window.AppConfig', window.AppConfig);
    this.loadEmployees();
  }

  async loadEmployees() {
    this.element('bbva-spinner').classList.remove('hide');
    const data = await this.employeeDM.listEmployee();
    this.element('list-employee').employees = data;
    this.element('bbva-spinner').classList.add('hide');
  }

  onPageLeave() {
    // Cada vez que salgamos de la pagina se ejecuta
  }

  static get styles() {
    return [styles, stylesPages];
  }
}

window.customElements.define(MainPage.is, MainPage);
