/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`.container {
  width: 100%;
  min-height: calc(100vh - 60px);
  display: flex;
  padding: 15px;
}
.container .content-form {
  width: 400px;
  padding: 0px;
}
.container .content-form h2 {
  font-size: 1.1em;
  margin: 0 10px;
  padding: 0;
  padding-bottom: 5px;
  border-bottom: 1px solid #e1e1e1;
}
.container .content-form .cmp-custom {
  --bg-form: #f4f4f4;
  --bg-color-input: white;
}
.container .content-list {
  width: calc(100% - 420px);
}

.loading {
  width: 100%;
  position: fixed;
  height: 100vh;
  z-index: 100;
  background-color: #fff;
  top: 0;
  left: 0;
}

.hide {
  display: none !important;
}
`;