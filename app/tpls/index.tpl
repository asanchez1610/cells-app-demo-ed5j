<!doctype html>

<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>cells-app-demo-ed5j</title>
  <!-- Place favicon.ico in the `app/` directory -->

  <!-- Chrome for Android theme color -->
  <meta name="theme-color" content="#303F9F">

  <!-- Web Application Manifest -->
  <link rel="manifest" href="manifest.json">

  <!-- Tile color for Win8 -->
  <meta name="msapplication-TileColor" content="#3372DF">

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="staticScaffold">
  <link rel="icon" sizes="192x192" href="resources/images/touch/chrome-touch-icon-192x192.png">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="staticScaffold">
  <link rel="apple-touch-icon" href="resources/images/touch/apple-touch-icon.png">

  <!-- Tile icon for Win8 (144x144) -->
  <meta name="msapplication-TileImage" content="resources/images/touch/ms-touch-icon-144x144-precomposed.png">

  <link rel="stylesheet" href="styles/main.css">
  <script>
    window.IntlMsg = window.IntlMsg || {};
    window.IntlMsg.lang = 'es-ES';
  </script>
</head>

<body class="fullbleed layout vertical loading">
  <div id="splash">
    <div class="content-loading">
      <div class="loadingio-spinner-blocks-ilt52kti4c"><div class="ldio-o2kanlkzodq">
        <div style='left:38px;top:38px;animation-delay:0s'></div><div style='left:80px;top:38px;animation-delay:0.125s'></div><div style='left:122px;top:38px;animation-delay:0.25s'></div><div style='left:38px;top:80px;animation-delay:0.875s'></div><div style='left:122px;top:80px;animation-delay:0.375s'></div><div style='left:38px;top:122px;animation-delay:0.75s'></div><div style='left:80px;top:122px;animation-delay:0.625s'></div><div style='left:122px;top:122px;animation-delay:0.5s'></div>
        </div>
      </div>
    </div>
  </div>
  <div id="app__content"></div>
  <aside class="menu-container">
    <bbva-web-navigation-menu></bbva-web-navigation-menu>
  </aside>
  <!-- for a11y purposes -->
  <div id="announcer" aria-live="polite"></div>
  <cells-access-control></cells-access-control>
  <cells-generic-service-dm accessControlCmp="cells-access-control" ></cells-generic-service-dm>
  <script src="vendor/bowser.min.js"></script>
  <script src="resources/js/index.js"></script>
  <script type="module" src="scripts/app-module.js"></script>
</body>

</html>
